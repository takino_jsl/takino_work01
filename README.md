# 社内PC管理

 create by JSL y.takino


## プログラム修正手順

1. manage.pyのデフォルトsettingsはmasterなので--settingsで指定する

    ```
    $ python manage.py **** --settings=pc_management.settings.develop
    ```
1. 修正が完了したら基本的にmasterにマージする

## デプロイ手順

1. heroku にログイン

    ```
    $ heroku login
    ```
1. heroku にPush

    ```
    $ git push heroku master
    # 別ブランチをPushする場合
    $ git push heroku [ブランチ]:master
    ```
1. migrateがある場合は手動で行う

    ```
    $ heroku run python manage.py migrate
    ```

## tips

1. アプリの情報を調べる(pc_managementのDynosはweb)

    ```
    $ heroku apps:info
    ```
1. アプリのページをブラウザで開く

    ```
    $ heroku open
    ```
1. アプリ名を変更する

    ```
    $ heroku rename pc-management
    ```
1. アプリの停止・起動

    ```
    heroku ps:scale web=0   # 停止
    heroku ps:scale web=1   # 起動
    ```
1. アプリのアクセスログを監視する

    ```
    $ heroku logs -t
    ```
1. アプリのメンテナンスモード

    ```
    $ heroku maintenance:on     # オン
    $ heroku maintenance:off    # オフ
    ```
1. DBのバックアップやリストア

    ```
    http://qiita.com/kimunny/items/c841bed5df73f0f93067
    ```
1. アプリを消す

    ```
    $ heroku apps:destroy --app pc-management --confirm pc-management
    ```