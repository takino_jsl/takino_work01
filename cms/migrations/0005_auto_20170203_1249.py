# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-02-03 03:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0004_auto_20160718_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pc_management',
            name='asset_class',
            field=models.IntegerField(choices=[(0, b'\xe8\xb3\xbc\xe5\x85\xa5\xe5\x93\x81'), (1, b'\xe3\x83\xaa\xe3\x83\xbc\xe3\x82\xb9\xe5\x93\x81')],
                                      default=0, null=True, verbose_name=b'\xe8\xb3\x87\xe7\x94\xa3\xe7\xa8\xae\xe9\xa1\x9e'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='display_number',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True,
                                   verbose_name=b'\xe3\x83\x87\xe3\x82\xa3\xe3\x82\xb9\xe3\x83\x97\xe3\x83\xac\xe3\x82\xa4 - \xe6\xa9\x9f\xe5\x99\xa8\xe7\x95\xaa\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='display_size',
            field=models.CharField(blank=True, max_length=255, null=True,
                                   verbose_name=b'\xe3\x83\x87\xe3\x82\xa3\xe3\x82\xb9\xe3\x83\x97\xe3\x83\xac\xe3\x82\xa4 - \xe3\x82\xb5\xe3\x82\xa4\xe3\x82\xba'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='hdd_storage',
            field=models.CharField(
                max_length=255, null=True, verbose_name=b'HDD\xe5\xae\xb9\xe9\x87\x8f'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='ip_address',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True,
                                   verbose_name=b'IP\xe3\x82\xa2\xe3\x83\x89\xe3\x83\xac\xe3\x82\xb9'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='keyboard_number',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True,
                                   verbose_name=b'\xe3\x82\xad\xe3\x83\xbc\xe3\x83\x9c\xe3\x83\xbc\xe3\x83\x89\xe6\xa9\x9f\xe5\x99\xa8\xe7\x95\xaa\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='lease_expiration_date',
            field=models.DateField(blank=True, max_length=255, null=True,
                                   verbose_name=b'\xe3\x83\xaa\xe3\x83\xbc\xe3\x82\xb9\xe6\x9c\x9f\xe9\x99\x90\xe6\x97\xa5'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='machine_type',
            field=models.IntegerField(choices=[(0, b'\xe3\x83\x87\xe3\x82\xb9\xe3\x82\xaf\xe3\x83\x88\xe3\x83\x83\xe3\x83\x97'), (
                1, b'\xe3\x83\x8e\xe3\x83\xbc\xe3\x83\x88\xe3\x83\x91\xe3\x82\xbd\xe3\x82\xb3\xe3\x83\xb3')], default=0, null=True, verbose_name=b'\xe7\xa8\xae\xe9\xa1\x9e'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='maker',
            field=models.CharField(
                max_length=255, null=True, verbose_name=b'\xe3\x83\xa1\xe3\x83\xbc\xe3\x82\xab\xe3\x83\xbc'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='media_drive',
            field=models.CharField(blank=True, max_length=255, null=True,
                                   verbose_name=b'\xe3\x83\xa1\xe3\x83\x87\xe3\x82\xa3\xe3\x82\xa2\xe3\x83\x89\xe3\x83\xa9\xe3\x82\xa4\xe3\x83\x96'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='memory_storage',
            field=models.CharField(
                max_length=255, null=True, verbose_name=b'\xe3\x83\xa1\xe3\x83\xa2\xe3\x83\xaa\xe5\xae\xb9\xe9\x87\x8f'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='mouth_number',
            field=models.CharField(blank=True, max_length=255, null=True, unique=True,
                                   verbose_name=b'\xe3\x83\x9e\xe3\x82\xa6\xe3\x82\xb9\xe6\xa9\x9f\xe5\x99\xa8\xe7\x95\xaa\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='pc_number',
            field=models.CharField(max_length=255, null=True, unique=True,
                                   verbose_name=b'PC\xe6\xa9\x9f\xe5\x99\xa8\xe7\x95\xaa\xe5\x8f\xb7'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='remarks',
            field=models.TextField(blank=True, null=True,
                                   verbose_name=b'\xe5\x82\x99\xe8\x80\x83'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='storage_location',
            field=models.CharField(blank=True, max_length=50, null=True,
                                   verbose_name=b'\xe4\xbf\x9d\xe7\xae\xa1\xe5\xa0\xb4\xe6\x89\x80'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='user',
            field=models.CharField(
                max_length=255, null=True, verbose_name=b'\xe4\xbd\xbf\xe7\x94\xa8\xe8\x80\x85'),
        ),
        migrations.AlterField(
            model_name='pc_management',
            name='user_project',
            field=models.CharField(blank=True, max_length=255, null=True,
                                   verbose_name=b'\xe4\xbd\xbf\xe7\x94\xa8\xe3\x83\x97\xe3\x83\xad\xe3\x82\xb8\xe3\x82\xa7\xe3\x82\xaf\xe3\x83\x88'),
        ),
    ]
