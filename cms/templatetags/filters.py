# coding: utf-8
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(name='truncate')
@stringfilter
def truncate(value, arg):
    return value[:int(arg)]


@register.filter(name='cutmark')
@stringfilter
def cutmark(value, arg='...(略)'):
    return value + arg
