# coding: utf-8
from django.conf.urls import url
from django.contrib.auth import views

from .views import *

urlpatterns = [
    # 社内PC管理
    url(r'^$', pc_list, name='pc_list'),   # 一覧
    url(r'^detail/(?P<pc_id>\d+)/$', pc_detail, name='pc_detail'),  # 詳細
    url(r'^add/$', pc_edit, name='pc_add'),  # 登録
    url(r'^mod/(?P<pc_id>\d+)/$', pc_edit, name='pc_mod'),  # 修正
    url(r'^del/(?P<pc_id>\d+)/$', pc_del, name='pc_del'),   # 削除
    url(r'^save_sort/$', save_sort, name='save_sort'),   # ソート順保存
    url(r'^download/$', download, name='download'),  # エクセルダウンロード
    url(r'^upload/$', upload, name='upload'),  # エクセルアップロード
    url(r'^login/$', views.login,
        {'template_name': 'cms/login.html'}, name='login'),   # ログイン
    url(r'^logout/$', views.logout,
        {'template_name': 'cms/logout.html'}, name='logout')  # ログアウト
]
