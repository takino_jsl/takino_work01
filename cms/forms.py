# coding: utf-8
from django import forms
from django.forms import ModelForm
from django.forms.widgets import DateInput, Select, Textarea, TextInput

from cms.models import PC_Management


class PC_ManagementForm(ModelForm):
    """PC追加編集フォーム."""
    class Meta:
        model = PC_Management
        fields = ('user', 'user_project', 'storage_location',
                  'machine_type', 'asset_class', 'pc_number', 'ip_address',
                  'maker', 'display_size', 'display_number', 'keyboard_number',
                  'mouth_number', 'os', 'cpu', 'hdd_storage', 'memory_storage',
                  'media_drive', 'lease_expiration_date', 'remarks', )
        widgets = {
            'user': TextInput(attrs={'size': 40}),
            'user_project': TextInput(attrs={'size': 40}),
            'lease_expiration_date': DateInput(attrs={'type': 'date'}),
        }


class PC_ReadOnlyForm(ModelForm):
    """PC閲覧専用フォーム."""
    class Meta:
        model = PC_Management
        fields = ('user', 'user_project', 'storage_location',
                  'machine_type', 'asset_class', 'pc_number', 'ip_address',
                  'maker', 'display_size', 'display_number', 'keyboard_number',
                  'mouth_number', 'os', 'cpu', 'hdd_storage', 'memory_storage',
                  'media_drive', 'lease_expiration_date', 'remarks', )
        widgets = {
            'sort_no': TextInput(attrs={'readonly': 'readonly'}),
            'user': TextInput(attrs={'size': 40, 'readonly': 'readonly'}),
            'user_project': TextInput(attrs={'size': 40, 'readonly': 'readonly'}),
            'storage_location': TextInput(attrs={'readonly': 'readonly'}),
            'machine_type': Select(attrs={'disabled': 'disabled'}),
            'asset_class': Select(attrs={'disabled': 'disabled'}),
            'pc_number': TextInput(attrs={'readonly': 'readonly'}),
            'ip_address': TextInput(attrs={'readonly': 'readonly'}),
            'maker': TextInput(attrs={'readonly': 'readonly'}),
            'display_size': TextInput(attrs={'readonly': 'readonly'}),
            'display_number': TextInput(attrs={'readonly': 'readonly'}),
            'keyboard_number': TextInput(attrs={'readonly': 'readonly'}),
            'mouth_number': TextInput(attrs={'readonly': 'readonly'}),
            'os': TextInput(attrs={'readonly': 'readonly'}),
            'cpu': TextInput(attrs={'readonly': 'readonly'}),
            'hdd_storage': TextInput(attrs={'readonly': 'readonly'}),
            'memory_storage': TextInput(attrs={'readonly': 'readonly'}),
            'media_drive': TextInput(attrs={'readonly': 'readonly'}),
            'lease_expiration_date': DateInput(attrs={'readonly': 'readonly', 'type': 'date', }, format=('%Y-%m-%d')),
            'remarks': Textarea(attrs={'readonly': 'readonly'}),
        }


class UploadFileForm(forms.Form):
    file = forms.FileField(label='Excel')
