# coding: utf-8
from django.contrib import admin

from cms.models import *


class PC_ManagementAdmin(admin.ModelAdmin):
    list_display = ('id', 'pc_number', 'user',
                    'user_project', 'remarks',)  # 一覧に出したい項目
    list_display_links = ('id', 'pc_number',)  # 修正リンクでクリックできる項目
admin.site.register(PC_Management, PC_ManagementAdmin)
