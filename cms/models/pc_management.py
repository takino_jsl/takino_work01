# coding: utf-8
from django.db import models


class NullableCharField(models.CharField):
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        if isinstance(value, models.CharField):
            return value
        return value or ''

    def get_prep_value(self, value):
        return value or None


class PC_Management(models.Model):

    MACHINE_TYPE_CHOICE = (
        (0, 'デスクトップ'),
        (1, 'ノートパソコン'),
    )
    ASSET_CLASS_CHOICE = (
        (0, '購入品'),
        (1, 'リース品'),
    )
    user = models.CharField('使用者', max_length=255, null=True)
    user_project = models.CharField(
        '使用プロジェクト', max_length=255, null=True, blank=True)
    storage_location = models.CharField(
        '保管場所', max_length=50, null=True, blank=True)
    machine_type = models.IntegerField(
        '種類', default=0, choices=MACHINE_TYPE_CHOICE, null=True)
    asset_class = models.IntegerField(
        '資産種類', default=0, choices=ASSET_CLASS_CHOICE, null=True)
    pc_number = models.CharField(
        'PC機器番号', max_length=255, null=True, unique=True)
    ip_address = NullableCharField(
        'IPアドレス', max_length=255, null=True, blank=True, unique=True)
    maker = models.CharField('メーカー', max_length=255, null=True)
    display_size = models.CharField(
        'ディスプレイ - サイズ', max_length=255, null=True, blank=True)
    display_number = NullableCharField(
        'ディスプレイ - 機器番号', max_length=255, null=True, blank=True, unique=True)
    keyboard_number = NullableCharField(
        'キーボード機器番号', max_length=255, null=True, blank=True, unique=True)
    mouth_number = NullableCharField(
        'マウス機器番号', max_length=255, null=True, blank=True, unique=True)
    os = models.CharField('OS', max_length=255, null=True)
    cpu = models.CharField('CPU', max_length=255, null=True)
    hdd_storage = models.CharField('HDD容量', max_length=255, null=True)
    memory_storage = models.CharField('メモリ容量', max_length=255, null=True)
    media_drive = models.CharField(
        'メディアドライブ', max_length=255, null=True, blank=True)
    lease_expiration_date = models.DateField(
        'リース期限日', max_length=255, null=True, blank=True)
    remarks = models.TextField('備考', null=True, blank=True)
    sort_no = models.IntegerField('ソートNo', unique=True)

    class Meta:
        verbose_name = '機器'
        verbose_name_plural = verbose_name
