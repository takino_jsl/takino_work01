# coding: utf-8
import datetime
import functools
import io
import os

import xlrd
import xlwt
import json
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render

from cms.forms import PC_ManagementForm, PC_ReadOnlyForm, UploadFileForm
from cms.models import PC_Management
from django.db.models import Max


def render_json_response(request, data, status=None):
    json_obj = json.dumps(data, ensure_ascii=False, indent=2)
    return HttpResponse(json_obj,
                        content_type='application/json; charset=UTF-8',
                        status=status)


def user_permission():
    def preprocess(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            # View の Request オブジェクトを取り出す
            request = args[0]
            # ユーザー権限を取得
            if not request.user.is_staff:
                raise Http404
            return func(*args, **kwargs)
        return wrapper
    return preprocess


@login_required
def pc_list(request):
    """PCの一覧."""
    pc_list = PC_Management.objects.all().order_by('sort_no')
    upload_form = UploadFileForm()
    return render(request,
                  'cms/pc_list.html',
                  {'pc_list': pc_list, 'upload_form': upload_form})


@login_required
def pc_detail(request, pc_id=None):
    """PCの詳細."""
    pc_detail = get_object_or_404(PC_Management, pk=pc_id)
    form = PC_ReadOnlyForm(instance=pc_detail)  # pc インスタンスからフォームを作成

    return render(request, 'cms/pc_detail.html', dict(form=form, pc_id=pc_id))


@login_required
@user_permission()
def pc_edit(request, pc_id=None):
    """PCの編集."""
    isCopy = request.GET.get('copy')
    if pc_id:  # pc_id が指定されている (修正時)
        pc = get_object_or_404(PC_Management, pk=pc_id)
        if isCopy:  # 複製の時は新規追加扱い
            pc.pk = None
            pc.id = None
    else:  # pc_id が指定されていない (追加時)
        pc = PC_Management()
    # 新規作成又は複製の時にソート番号を割り当てる
    if not pc_id or isCopy:
        pc.sort_no = PC_Management.objects.all().aggregate(
            Max('sort_no'))['sort_no__max'] + 1

    if request.method == 'POST':
        # POST された request データからフォームを作成
        form = PC_ManagementForm(request.POST, instance=pc)
        if form.is_valid():  # フォームのバリデーション
            pc = form.save(commit=False)
            pc.save()
            return redirect('cms:pc_list')
    else:  # GET の時
        form = PC_ManagementForm(instance=pc)  # pc インスタンスからフォームを作成

    return render(request, 'cms/pc_edit.html', dict(form=form, pc_id=pc_id, isCopy=isCopy))


@login_required
@user_permission()
def save_sort(request):
    """ソート"""
    ids = request.GET.getlist('ids[]')  # クエリーストリングの配列を取得
    res = {}    # id: sort_no
    for i, pk in enumerate(ids):
        pc = PC_Management.objects.get(pk=pk)
        pc.sort_no = i + 1
        pc.save()
        res.update({pc.id: pc.sort_no})
    return render_json_response(request, res, 200)


@login_required
@user_permission()
def pc_del(request, pc_id):
    """PCの削除."""
    pc = get_object_or_404(PC_Management, pk=pc_id)
    pc.delete()
    return redirect('cms:pc_list')


@login_required
@user_permission()
def download(request):
    output = io.BytesIO()

    wb = xlwt.Workbook()
    ws = wb.add_sheet('sheet 1')

    pc_list = PC_Management.objects.all().order_by('id')
    i = 0
    for pc in pc_list:
        if i == 0:
            ws.write(i, 0, '使用者')
            ws.write(i, 1, '使用プロジェクト')
            ws.write(i, 2, '保管場所')
            ws.write(i, 3, '種類(0:デスクトップパソコン 1:ノートパソコン)')
            ws.write(i, 4, '資産種類(0:購入品 1:リース品)')
            ws.write(i, 5, 'PC機器番号')
            ws.write(i, 6, 'IPアドレス')
            ws.write(i, 7, 'メーカー')
            ws.write(i, 8, 'ディスプレイ - サイズ')
            ws.write(i, 9, 'ディスプレイ - 機器番号')
            ws.write(i, 10, 'キーボード機器番号')
            ws.write(i, 11, 'マウス機器番号')
            ws.write(i, 12, 'OS')
            ws.write(i, 13, 'CPU')
            ws.write(i, 14, 'HDD容量')
            ws.write(i, 15, 'メモリ容量')
            ws.write(i, 16, 'メディアドライブ')
            ws.write(i, 17, 'リース期限日')
            ws.write(i, 18, '備考')
            i += 1
        ws.write(i, 0, pc.user)
        ws.write(i, 1, pc.user_project)
        ws.write(i, 2, pc.storage_location)
        ws.write(i, 3, pc.machine_type)
        ws.write(i, 4, pc.asset_class)
        ws.write(i, 5, pc.pc_number)
        ws.write(i, 6, pc.ip_address)
        ws.write(i, 7, pc.maker)
        ws.write(i, 8, pc.display_size)
        ws.write(i, 9, pc.display_number)
        ws.write(i, 10, pc.keyboard_number)
        ws.write(i, 11, pc.mouth_number)
        ws.write(i, 12, pc.os)
        ws.write(i, 13, pc.cpu)
        ws.write(i, 14, pc.hdd_storage)
        ws.write(i, 15, pc.memory_storage)
        ws.write(i, 16, pc.media_drive)
        ws.write(i, 17, pc.lease_expiration_date,
                 xlwt.easyxf(num_format_str='yyyy-mm-dd'))
        ws.write(i, 18, pc.remarks)
        i += 1
    wb.save(output)

    response = HttpResponse(
        output.getvalue(), content_type='application/excel')
    response['Content-Disposition'] = 'filename=pc_list.xls'
    return response


@login_required
@user_permission()
def upload(request):
    if request.method == 'POST':
        upload_form = UploadFileForm(request.POST, request.FILES)
        if upload_form.is_valid():
            excel_registration(request.FILES['file'])
            return redirect('cms:pc_list')
    else:
        return redirect('cms:pc_list')


def excel_registration(f):

    with open(settings.MEDIA_ROOT + '/' + f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    with xlrd.open_workbook(settings.MEDIA_ROOT + '/' + f.name) as excel:

        for i in range(excel.nsheets):
            sheet = excel.sheet_by_index(i)
            for row in range(1, sheet.nrows):
                pc = PC_Management()
                pc.user = sheet.cell(row, 0).value
                pc.user_project = sheet.cell(row, 1).value
                pc.storage_location = sheet.cell(row, 2).value
                pc.machine_type = sheet.cell(row, 3).value
                pc.asset_class = sheet.cell(row, 4).value
                pc.pc_number = sheet.cell(row, 5).value
                pc.ip_address = sheet.cell(row, 6).value
                pc.maker = sheet.cell(row, 7).value
                pc.display_size = sheet.cell(row, 8).value
                pc.display_number = sheet.cell(row, 9).value
                pc.keyboard_number = sheet.cell(row, 10).value
                pc.mouth_number = sheet.cell(row, 11).value
                pc.os = sheet.cell(row, 12).value
                pc.cpu = sheet.cell(row, 13).value
                pc.hdd_storage = sheet.cell(row, 14).value
                pc.memory_storage = sheet.cell(row, 15).value
                pc.media_drive = sheet.cell(row, 16).value
                if sheet.cell(row, 17).value:
                    pc.lease_expiration_date = datetime.datetime(
                        *xlrd.xldate_as_tuple(sheet.cell(row, 17).value, excel.datemode))
                pc.remarks = sheet.cell(row, 18).value
                pc.save()
    os.remove(settings.MEDIA_ROOT + '/' + f.name)
