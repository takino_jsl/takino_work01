# coding: utf-8
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'workshop',
        'USER': 'workshop',
        'PASSWORD': 'Yj7oPM83kP',
        'HOST': 'localhost',
        'PORT': '3306'
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

# 静的ファイルを共通で置く
STATIC_URL = '/static/'  # 配信用のURL
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
